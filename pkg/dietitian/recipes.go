package dietitian

import (
	"context"
	"fmt"

	"gitlab.com/nutrimate/dietitian/pkg/cookbook"
	"gitlab.com/nutrimate/dietitian/pkg/recipe"
)

// TODO: should this be here? maybe this should be in a patient.Service ?

// RecipesService is a service for accessing recipes by a dietitian
type RecipesService interface {
	GetAllForPatient(ctx context.Context) ([]recipe.Recipe, error)
}

type recipes struct {
	cookbook cookbook.Client
}

// GetAllForPatient returns all recipes allowed for a patient
func (r recipes) GetAllForPatient(ctx context.Context) ([]recipe.Recipe, error) {
	recipes, err := r.cookbook.GetAllRecipes(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to load recipes from cookbook: %w", err)
	}

	return recipes, nil
}

// NewRecipesService creates a new RecipeService
func NewRecipesService(cookbookClient cookbook.Client) RecipesService {
	return recipes{
		cookbook: cookbookClient,
	}
}
