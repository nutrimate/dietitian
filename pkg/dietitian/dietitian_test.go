package dietitian_test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/dietitian/pkg/dietitian"
	"gitlab.com/nutrimate/dietitian/pkg/patient"
)

func TestDietitian_WorksWithPatient(t *testing.T) {
	testCases := []struct {
		name      string
		worksWith []patient.ID
		query     patient.ID
		expected  bool
	}{
		{
			name: "should return false when dietitian does not work with patient",
			worksWith: []patient.ID{
				patient.ID(uuid.New()),
				patient.ID(uuid.New()),
			},
			query:    patient.ID(uuid.New()),
			expected: false,
		},
		{
			name: "should return true when dietitian works with patient",
			worksWith: []patient.ID{
				patient.ID(uuid.New()),
				patient.ID(uuid.MustParse("10000000-0000-0000-0000-000000000001")),
			},
			query:    patient.ID(uuid.MustParse("10000000-0000-0000-0000-000000000001")),
			expected: true,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			d := dietitian.Dietitian{
				ID:       dietitian.ID(uuid.New()),
				UserID:   "user-id",
				Patients: tc.worksWith,
			}

			result := d.WorksWithPatient(tc.query)

			assert.Equal(t, tc.expected, result)
		})
	}
}
