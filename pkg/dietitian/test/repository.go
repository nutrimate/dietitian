package dietitiantest

import (
	"context"

	"github.com/stretchr/testify/mock"

	"gitlab.com/nutrimate/dietitian/pkg/recipe"
)

// MockRecipesService is a mock of dietitian.Recipes
type MockRecipesService struct {
	mock.Mock
}

// GetAllForPatient is a mock method
func (m *MockRecipesService) GetAllForPatient(ctx context.Context) ([]recipe.Recipe, error) {
	args := m.Called(ctx)
	return args.Get(0).([]recipe.Recipe), args.Error(1)
}
