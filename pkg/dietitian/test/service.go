package dietitiantest

import (
	"github.com/stretchr/testify/mock"

	"gitlab.com/nutrimate/dietitian/pkg/dietitian"
)

// MockService is a mock of dietitian.Service
type MockService struct {
	mock.Mock
}

// GetByUserID is a mock method
func (m *MockService) GetByUserID(userID string) (dietitian.Dietitian, error) {
	args := m.Called(userID)

	return args.Get(0).(dietitian.Dietitian), args.Error(1)
}
