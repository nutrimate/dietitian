package dietitian

import (
	"github.com/google/uuid"

	"gitlab.com/nutrimate/dietitian/pkg/patient"
)

// Service is a service used for working with dietitians
type Service interface {
	GetByUserID(userID string) (Dietitian, error)
}

type service struct {
}

// GetByUserID returns a mock dietitian with given user id
func (s service) GetByUserID(userID string) (Dietitian, error) {
	dietitian := Dietitian{
		ID:     ID(uuid.MustParse("00000000-0000-0000-0000-000000000001")),
		UserID: userID,
		Patients: []patient.ID{
			patient.ID(uuid.MustParse("00000000-0000-0000-0000-000000000002")),
			patient.ID(uuid.MustParse("00000000-0000-0000-0000-000000000003")),
		},
	}

	return dietitian, nil
}

// NewService returns new instance of dietitian service
func NewService() Service {
	return service{}
}
