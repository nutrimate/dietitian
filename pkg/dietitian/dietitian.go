package dietitian

import (
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/nutrimate/dietitian/pkg/domainerrors"
	"gitlab.com/nutrimate/dietitian/pkg/patient"
)

// ErrNoAccessToPatient is an error used when dietitian has no access to a patient
var ErrNoAccessToPatient = fmt.Errorf("%w: dietitian has no access to patient", domainerrors.ErrForbidden)

// ID is an UUID that identifies a dietitian
type ID uuid.UUID

// Dietitian represents a user in dietitian role
type Dietitian struct {
	ID ID

	// UserID is a unique identifier that ties user provided by identity management server with dietitian entity
	UserID string

	Patients []patient.ID
}

// WorksWithPatient returns true when dietitian works with a given patient or false otherwise
func (d Dietitian) WorksWithPatient(patientID patient.ID) bool {
	for _, id := range d.Patients {
		if id == patientID {
			return true
		}
	}

	return false
}
