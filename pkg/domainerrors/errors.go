package domainerrors

import "errors"

var (
	// ErrValidation is used for indicating that provided values do not conform to business validation rules
	ErrValidation = errors.New("validation error")
	// ErrForbidden is an error used when action is forbidden
	ErrForbidden = errors.New("forbidden")
)
