package recipetest

import (
	"github.com/google/uuid"

	"gitlab.com/nutrimate/dietitian/pkg/recipe"
)

// CreateTestRecipe creates a new recipe with pre-filled values
func CreateTestRecipe() recipe.Recipe {
	return recipe.Recipe{
		ID: recipe.ID(uuid.New()),
		MealTypeIDs: []recipe.MealTypeID{
			recipe.MealTypeID(uuid.New()),
		},
		Calories:      2000,
		Proteins:      60,
		Fats:          40,
		Carbohydrates: 80,
	}
}
