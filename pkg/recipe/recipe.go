package recipe

import "github.com/google/uuid"

// ID is an UUID that identifies a recipes
type ID uuid.UUID

// MealTypeID is an UUID that identifies a MealType
type MealTypeID uuid.UUID

// Recipe is a condensed representation of a Recipe from cookbook
type Recipe struct {
	ID            ID
	MealTypeIDs   []MealTypeID
	Calories      float64
	Proteins      float64
	Fats          float64
	Carbohydrates float64
}
