package diet_test

import (
	"testing"

	"cloud.google.com/go/civil"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/nutrimate/dietitian/pkg/diet"
)

func mustParseDate(s string) civil.Date {
	d, err := civil.ParseDate(s)
	if err != nil {
		panic(err)
	}

	return d
}

func TestNewInterval(t *testing.T) {
	t.Run("should return error when start day is after end date", func(t *testing.T) {
		start := mustParseDate("2021-02-20")
		end := mustParseDate("2021-02-10")

		_, err := diet.NewInterval(start, end)

		assert.Equal(t, diet.ErrInvalidInterval, err)
	})

	t.Run("should return error when interval is greater than 14 days", func(t *testing.T) {
		start := mustParseDate("2021-02-24")
		end := mustParseDate("2021-02-10")

		_, err := diet.NewInterval(start, end)

		assert.Equal(t, diet.ErrInvalidInterval, err)
	})

	t.Run("should create interval for valid dates", func(t *testing.T) {
		start := mustParseDate("2021-02-10")
		end := mustParseDate("2021-02-20")

		interval, err := diet.NewInterval(start, end)

		if assert.NoError(t, err) {
			assert.Equal(t, start, interval.StartDay)
			assert.Equal(t, end, interval.EndDay)
		}
	})
}

func TestInterval_Days(t *testing.T) {
	t.Run("should return difference in days between start and end", func(t *testing.T) {
		start := mustParseDate("2021-02-10")
		end := mustParseDate("2021-02-19")
		interval, err := diet.NewInterval(start, end)
		require.NoError(t, err)

		days := interval.Days()

		assert.Equal(t, uint8(10), days)
	})
}
