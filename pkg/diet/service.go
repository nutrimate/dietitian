package diet

import (
	"context"
	"fmt"

	"gitlab.com/nutrimate/dietitian/pkg/diet/commands"
	"gitlab.com/nutrimate/dietitian/pkg/dietitian"
	"gitlab.com/nutrimate/dietitian/pkg/mealplanner"
	"gitlab.com/nutrimate/dietitian/pkg/patient"
	"gitlab.com/nutrimate/dietitian/pkg/recipe"
)

// Service is an application service for working with diets
type Service interface {
	CreateNewDiet(ctx context.Context, command commands.CreateNewDiet) error
}

type service struct {
	diets       Repository
	dietitians  dietitian.Service
	recipes     dietitian.RecipesService
	mealPlanner mealplanner.MealPlanner
}

// CreateNewDiet generates a new diet using meal planner and saves it in the repository
func (s service) CreateNewDiet(ctx context.Context, command commands.CreateNewDiet) error {
	requestingDietitian, err := s.dietitians.GetByUserID(command.UserID)
	if err != nil {
		return fmt.Errorf("could not get dietitian: %w", err)
	}

	patientID := patient.ID(command.PatientID)
	if !requestingDietitian.WorksWithPatient(patientID) {
		return dietitian.ErrNoAccessToPatient
	}

	days, err := mealplanner.NewDays(command.NumberOfDays)
	if err != nil {
		return err
	}

	goal, err := mealplanner.NewDietGoal(command.Goal)
	if err != nil {
		return err
	}

	mealSchedule := make([]recipe.MealTypeID, len(command.MealSchedule))
	for i, m := range command.MealSchedule {
		mealSchedule[i] = recipe.MealTypeID(m)
	}

	recipes, err := s.recipes.GetAllForPatient(ctx)
	if err != nil {
		return fmt.Errorf("failed to get recipes for patient: %w", err)
	}

	request := mealplanner.PlanDietRequest{
		Recipes:            recipes,
		MealSchedule:       mealSchedule,
		DailyCalorieIntake: command.DailyCalorieIntake,
		DailyMinProteins:   command.DailyMinProteins,
		DailyMaxProteins:   command.DailyMaxProteins,
		DailyMinFats:       command.DailyMinFats,
		Days:               days,
		Goal:               goal,
	}

	dietEntries, err := s.mealPlanner.PlanDiet(ctx, request)
	if err != nil {
		return err
	}

	mealPlanEntries := make([]MealPlanEntry, len(dietEntries))
	for i, e := range dietEntries {
		mealPlanEntries[i] = MealPlanEntry{
			Day:        e.Day,
			MealTypeID: recipe.MealTypeID(e.MealTypeID),
			RecipeID:   recipe.ID(e.RecipeID),
		}
	}

	endDay := command.StartDay.AddDays(int(command.NumberOfDays) - 1)
	interval, err := NewInterval(command.StartDay, endDay)
	if err != nil {
		return err
	}

	builder := Builder{
		ID:                 ID(command.DietID),
		PatientID:          patientID,
		Interval:           interval,
		MealSchedule:       mealSchedule,
		DailyCalorieIntake: command.DailyCalorieIntake,
		MealPlanEntries:    mealPlanEntries,
	}

	diet, err := New(builder)
	if err != nil {
		return err
	}

	return s.diets.Create(ctx, diet)
}

// NewService creates a new application service for working with diets
func NewService(dietRepository Repository, dietitianService dietitian.Service, recipesService dietitian.RecipesService, mealPlanner mealplanner.MealPlanner) Service {
	return service{
		diets:       dietRepository,
		dietitians:  dietitianService,
		recipes:     recipesService,
		mealPlanner: mealPlanner,
	}
}
