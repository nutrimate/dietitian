package diet

import (
	"github.com/google/uuid"

	"gitlab.com/nutrimate/dietitian/pkg/patient"
	"gitlab.com/nutrimate/dietitian/pkg/recipe"
)

// ID is an UUID that identifies a diet
type ID uuid.UUID

// Diet is a diet for a user which spans given week
type Diet struct {
	ID                 ID
	PatientID          patient.ID
	Interval           Interval
	MealSchedule       MealSchedule
	DailyCalorieIntake float64
	MealPlan           MealPlan
}

// Builder contains values necessary to create a Diet
type Builder struct {
	ID                 ID
	PatientID          patient.ID
	Interval           Interval
	MealSchedule       []recipe.MealTypeID
	DailyCalorieIntake float64
	MealPlanEntries    []MealPlanEntry
}

// New creates a new Diet
func New(builder Builder) (Diet, error) {
	mealSchedule, err := NewMealSchedule(builder.MealSchedule)
	if err != nil {
		return Diet{}, err
	}

	mealPlan, err := NewMealPlan(builder.MealPlanEntries, builder.Interval.Days(), mealSchedule)
	if err != nil {
		return Diet{}, err
	}

	d := Diet{
		ID:                 builder.ID,
		PatientID:          builder.PatientID,
		Interval:           builder.Interval,
		MealSchedule:       mealSchedule,
		DailyCalorieIntake: builder.DailyCalorieIntake,
		MealPlan:           mealPlan,
	}

	return d, nil
}
