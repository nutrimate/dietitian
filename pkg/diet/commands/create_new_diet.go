package commands

import (
	"cloud.google.com/go/civil"
	"github.com/google/uuid"
)

// CreateNewDiet is a command used to create diets from scratch
type CreateNewDiet struct {
	UserID             string
	DietID             uuid.UUID
	PatientID          uuid.UUID
	MealSchedule       []uuid.UUID
	StartDay           civil.Date
	NumberOfDays       uint8
	Goal               string
	DailyCalorieIntake float64
	DailyMinProteins   float64
	DailyMaxProteins   float64
	DailyMinFats       float64
}
