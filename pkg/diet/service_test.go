package diet_test

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/nutrimate/dietitian/pkg/diet"
	"gitlab.com/nutrimate/dietitian/pkg/diet/commands"
	diettest "gitlab.com/nutrimate/dietitian/pkg/diet/test"
	"gitlab.com/nutrimate/dietitian/pkg/dietitian"
	dietitiantest "gitlab.com/nutrimate/dietitian/pkg/dietitian/test"
	"gitlab.com/nutrimate/dietitian/pkg/mealplanner"
	mealplannertest "gitlab.com/nutrimate/dietitian/pkg/mealplanner/test"
	"gitlab.com/nutrimate/dietitian/pkg/patient"
	"gitlab.com/nutrimate/dietitian/pkg/recipe"
	recipetest "gitlab.com/nutrimate/dietitian/pkg/recipe/test"
)

func TestService_CreateNewDiet(t *testing.T) {
	mealTypeIDs := []uuid.UUID{
		uuid.New(),
		uuid.New(),
	}
	mealSchedule := diet.MealSchedule{
		recipe.MealTypeID(mealTypeIDs[0]),
		recipe.MealTypeID(mealTypeIDs[1]),
	}
	recipes := []recipe.Recipe{
		recipetest.CreateTestRecipe(),
		recipetest.CreateTestRecipe(),
		recipetest.CreateTestRecipe(),
	}

	t.Run("should generate a diet and save it in database", func(t *testing.T) {
		// Given
		userID := "user-id"
		dietID := uuid.New()
		patientID := uuid.New()
		generatedDietEntries := []mealplanner.DietEntry{
			{Day: 1, RecipeID: uuid.New(), MealTypeID: mealTypeIDs[0]},
			{Day: 1, RecipeID: uuid.New(), MealTypeID: mealTypeIDs[1]},
			{Day: 2, RecipeID: uuid.New(), MealTypeID: mealTypeIDs[0]},
			{Day: 2, RecipeID: uuid.New(), MealTypeID: mealTypeIDs[1]},
			{Day: 3, RecipeID: uuid.New(), MealTypeID: mealTypeIDs[0]},
			{Day: 3, RecipeID: uuid.New(), MealTypeID: mealTypeIDs[1]},
		}

		mockDietitian := dietitian.Dietitian{
			ID:     dietitian.ID(uuid.New()),
			UserID: userID,
			Patients: []patient.ID{
				patient.ID(patientID),
			},
		}

		mealPlanner := &mealplannertest.MockMealPlanner{}
		dietRepository := &diettest.MockRepository{}
		dietitianRecipes := &dietitiantest.MockRecipesService{}
		dietitianService := &dietitiantest.MockService{}
		service := diet.NewService(dietRepository, dietitianService, dietitianRecipes, mealPlanner)
		ctx := context.Background()

		mealPlanner.On("PlanDiet", ctx, mock.IsType(mealplanner.PlanDietRequest{})).
			Return(generatedDietEntries, nil)
		dietRepository.On("Create", ctx, mock.IsType(diet.Diet{})).
			Return(nil)
		dietitianRecipes.On("GetAllForPatient", ctx).
			Return(recipes, nil)
		dietitianService.On("GetByUserID", userID).
			Return(mockDietitian, nil)

		command := commands.CreateNewDiet{
			DietID:             dietID,
			PatientID:          patientID,
			UserID:             userID,
			MealSchedule:       mealTypeIDs,
			StartDay:           mustParseDate("2020-02-10"),
			NumberOfDays:       3,
			Goal:               mealplanner.LoseWeightDietGoal,
			DailyCalorieIntake: 2000,
			DailyMinProteins:   80,
			DailyMaxProteins:   200,
			DailyMinFats:       60,
		}

		// When
		err := service.CreateNewDiet(ctx, command)

		// Then
		assert.NoError(t, err)
		mealPlanner.AssertCalled(t, "PlanDiet", ctx, mealplanner.PlanDietRequest{
			Recipes:            recipes,
			MealSchedule:       mealSchedule,
			DailyCalorieIntake: command.DailyCalorieIntake,
			DailyMinProteins:   command.DailyMinProteins,
			DailyMaxProteins:   command.DailyMaxProteins,
			DailyMinFats:       command.DailyMinFats,
			Days:               mealplanner.Days(command.NumberOfDays),
			Goal:               mealplanner.DietGoal(command.Goal),
		})
		expectedMealPlan := diet.MealPlan{}
		for _, e := range generatedDietEntries {
			expectedMealPlan = append(expectedMealPlan, diet.MealPlanEntry{
				Day:        e.Day,
				MealTypeID: recipe.MealTypeID(e.MealTypeID),
				RecipeID:   recipe.ID(e.RecipeID),
			})
		}
		dietRepository.AssertCalled(t, "Create", ctx, diet.Diet{
			ID:        diet.ID(dietID),
			PatientID: patient.ID(patientID),
			Interval: diet.Interval{
				StartDay: command.StartDay,
				EndDay:   command.StartDay.AddDays(int(command.NumberOfDays) - 1),
			},
			MealSchedule:       mealSchedule,
			DailyCalorieIntake: command.DailyCalorieIntake,
			MealPlan:           expectedMealPlan,
		})
	})
}
