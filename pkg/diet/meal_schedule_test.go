package diet_test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/dietitian/pkg/diet"
	"gitlab.com/nutrimate/dietitian/pkg/recipe"
)

func TestNewMealSchedule(t *testing.T) {
	t.Run("should return error when there are duplicate meal types in meal schedule", func(t *testing.T) {
		mealTypes := []recipe.MealTypeID{
			recipe.MealTypeID(uuid.New()),
			recipe.MealTypeID(uuid.New()),
		}
		mealTypes = append(mealTypes, mealTypes[0])

		_, err := diet.NewMealSchedule(mealTypes)

		assert.Equal(t, diet.ErrDuplicateMealInSchedule, err)
	})

	t.Run("should return meal schedule based on input", func(t *testing.T) {
		x := recipe.MealTypeID(uuid.New())
		mealTypes := []recipe.MealTypeID{
			x,
			recipe.MealTypeID(uuid.New()),
			recipe.MealTypeID(uuid.New()),
		}

		mealSchedule, err := diet.NewMealSchedule(mealTypes)

		if assert.NoError(t, err) {
			assert.Equal(t, mealTypes, []recipe.MealTypeID(mealSchedule))
		}
	})
}
