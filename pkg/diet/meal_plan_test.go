package diet_test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/dietitian/pkg/diet"
	"gitlab.com/nutrimate/dietitian/pkg/recipe"
)

func TestNewMealPlan(t *testing.T) {
	mealTypes := []recipe.MealTypeID{
		recipe.MealTypeID(uuid.New()),
		recipe.MealTypeID(uuid.New()),
	}
	mealSchedule, err := diet.NewMealSchedule(mealTypes)
	if err != nil {
		panic(err)
	}
	recipeID := recipe.ID(uuid.New())

	t.Run("should return error when a recipe is not included for a given meal", func(t *testing.T) {
		entries := []diet.MealPlanEntry{
			{Day: 1, RecipeID: recipeID, MealTypeID: mealTypes[0]},
			{Day: 1, RecipeID: recipeID, MealTypeID: mealTypes[1]},
			{Day: 2, RecipeID: recipeID, MealTypeID: mealTypes[1]},
			{Day: 3, RecipeID: recipeID, MealTypeID: mealTypes[0]},
			{Day: 3, RecipeID: recipeID, MealTypeID: mealTypes[1]},
		}

		_, err = diet.NewMealPlan(entries, 2, mealSchedule)

		assert.ErrorIs(t, err, diet.ErrInvalidMealPlan)
	})

	t.Run("should return meal plan", func(t *testing.T) {
		entries := []diet.MealPlanEntry{
			{Day: 1, RecipeID: recipeID, MealTypeID: mealTypes[0]},
			{Day: 1, RecipeID: recipeID, MealTypeID: mealTypes[1]},
			{Day: 2, RecipeID: recipeID, MealTypeID: mealTypes[0]},
			{Day: 2, RecipeID: recipeID, MealTypeID: mealTypes[1]},
			{Day: 3, RecipeID: recipeID, MealTypeID: mealTypes[0]},
			{Day: 3, RecipeID: recipeID, MealTypeID: mealTypes[1]},
		}

		result, err := diet.NewMealPlan(entries, 3, mealSchedule)

		if assert.NoError(t, err) {
			assert.Equal(t, entries, []diet.MealPlanEntry(result))
		}
	})
}
