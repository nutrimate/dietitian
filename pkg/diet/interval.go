package diet

import (
	"fmt"

	"cloud.google.com/go/civil"

	"gitlab.com/nutrimate/dietitian/pkg/domainerrors"
)

// ErrInvalidInterval is a validation error returned when date interval is invalid
var ErrInvalidInterval = fmt.Errorf("%w: invalid diet interval", domainerrors.ErrValidation)

// Interval represents the time period the diet spans for
type Interval struct {
	StartDay civil.Date
	EndDay   civil.Date
}

// Days returns number of days the diet is planned for
func (i Interval) Days() uint8 {
	days := i.EndDay.DaysSince(i.StartDay) + 1
	return uint8(days)
}

// NewInterval creates new interval spanning from start to end (inclusive)
func NewInterval(start civil.Date, end civil.Date) (Interval, error) {
	if end.Before(start) {
		return Interval{}, ErrInvalidInterval
	}

	if end.DaysSince(start) == 0 || end.DaysSince(start)+1 > 14 {
		return Interval{}, ErrInvalidInterval
	}

	i := Interval{
		StartDay: start,
		EndDay:   end,
	}

	return i, nil
}
