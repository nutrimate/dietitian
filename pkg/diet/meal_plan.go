package diet

import (
	"fmt"

	"gitlab.com/nutrimate/dietitian/pkg/domainerrors"
	"gitlab.com/nutrimate/dietitian/pkg/recipe"
)

// MealPlanEntry describes which recipe should be used to prepare a dish for a meal on a given day
type MealPlanEntry struct {
	Day        uint8
	MealTypeID recipe.MealTypeID
	RecipeID   recipe.ID
}

// MealPlan describes what should be eaten for each meal on consecutive days of diet
type MealPlan []MealPlanEntry

// ErrInvalidMealPlan is a validation error returned when meal plan is invalid
var ErrInvalidMealPlan = fmt.Errorf("%w: meal plan is invalid", domainerrors.ErrValidation)

// NewMealPlan creates new MealPlan from MealPlanEntries validating them against planned number of days and meal schedule
func NewMealPlan(entries []MealPlanEntry, numberOfDays uint8, mealSchedule MealSchedule) (MealPlan, error) {
	dailyEntries := make(map[uint8]map[recipe.MealTypeID]recipe.ID, numberOfDays)

	for _, e := range entries {
		if _, ok := dailyEntries[e.Day]; !ok {
			dailyEntries[e.Day] = make(map[recipe.MealTypeID]recipe.ID, len(mealSchedule))
		}

		dailyEntries[e.Day][e.MealTypeID] = e.RecipeID
	}

	if len(dailyEntries) != int(numberOfDays) {
		return nil, fmt.Errorf("%w: number of days does not match", ErrInvalidMealPlan)
	}

	for d := uint8(1); d <= numberOfDays; d++ {
		entriesForDay, ok := dailyEntries[d]
		if !ok {
			return nil, fmt.Errorf("%w: could not find entry for day %d", ErrInvalidMealPlan, d)
		}

		if len(entriesForDay) != len(mealSchedule) {
			return nil, fmt.Errorf("%w: number of meals does not match on day %d", ErrInvalidMealPlan, d)
		}

		for _, mealTypeID := range mealSchedule {
			if _, ok := entriesForDay[mealTypeID]; !ok {
				return nil, fmt.Errorf("%w: could not find entry for meal %s on day %d", ErrInvalidMealPlan, mealTypeID, d)
			}
		}
	}

	return entries, nil
}
