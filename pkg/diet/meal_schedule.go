package diet

import (
	"fmt"

	"gitlab.com/nutrimate/dietitian/pkg/domainerrors"
	"gitlab.com/nutrimate/dietitian/pkg/recipe"
)

var (
	// ErrMealScheduleEmpty is a validation error returned when meal schedule is empty
	ErrMealScheduleEmpty = fmt.Errorf("%w: meal schedule is empty", domainerrors.ErrValidation)
	// ErrDuplicateMealInSchedule is a validation error returned when meal schedule contains duplicate entries
	ErrDuplicateMealInSchedule = fmt.Errorf("%w: duplicate meal in meal schedule", domainerrors.ErrValidation)
)

// MealSchedule is an ordered collection of unique MealTypes eaten each day on a diet
type MealSchedule []recipe.MealTypeID

// NewMealSchedule creates MealSchedule from provided meal types validating their uniqueness
func NewMealSchedule(mealTypes []recipe.MealTypeID) (MealSchedule, error) {
	if len(mealTypes) == 0 {
		return nil, ErrMealScheduleEmpty
	}

	previousMealTypes := make(map[recipe.MealTypeID]interface{})

	for _, id := range mealTypes {
		if _, exists := previousMealTypes[id]; exists {
			return nil, ErrDuplicateMealInSchedule
		}

		previousMealTypes[id] = nil
	}

	return append(MealSchedule(nil), mealTypes...), nil
}
