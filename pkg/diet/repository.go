package diet

import "context"

// Repository is a generic repository for accessing diets
type Repository interface {
	Create(ctx context.Context, diet Diet) error
}
