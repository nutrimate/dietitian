package diettest

import (
	"context"

	"github.com/stretchr/testify/mock"

	"gitlab.com/nutrimate/dietitian/pkg/diet/commands"
)

// MockService is a mock of diet.Service
type MockService struct {
	mock.Mock
}

// CreateNewDiet is a mock method
func (m *MockService) CreateNewDiet(ctx context.Context, command commands.CreateNewDiet) error {
	args := m.Called(ctx, command)

	return args.Error(0)
}
