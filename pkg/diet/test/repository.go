package diettest

import (
	"context"

	"github.com/stretchr/testify/mock"

	"gitlab.com/nutrimate/dietitian/pkg/diet"
)

// MockRepository is a mock of diet.Repository
type MockRepository struct {
	mock.Mock
}

// Create is a mock method
func (m *MockRepository) Create(ctx context.Context, diet diet.Diet) error {
	args := m.Called(ctx, diet)

	return args.Error(0)
}
