package mealplanner

import (
	"gitlab.com/nutrimate/dietitian/pkg/recipe"
)

// PlanDietRequest describes input parameters to diet planning
type PlanDietRequest struct {
	Recipes            []recipe.Recipe     `json:"recipes"`
	MealSchedule       []recipe.MealTypeID `json:"mealSchedule"`
	DailyCalorieIntake float64             `json:"dailyCalorieIntake"`
	DailyMinProteins   float64             `json:"dailyMinProteins"`
	DailyMaxProteins   float64             `json:"dailyMaxProteins"`
	DailyMinFats       float64             `json:"dailyMinFats"`
	Days               Days                `json:"days"`
	Goal               DietGoal            `json:"goal"`
}
