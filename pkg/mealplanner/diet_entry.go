package mealplanner

import "github.com/google/uuid"

// DietEntry describe what recipe should be used to prepare a dish for a meal on a specific day
type DietEntry struct {
	Day        uint8     `json:"day"`
	RecipeID   uuid.UUID `json:"recipeId"`
	MealTypeID uuid.UUID `json:"mealTypeId"`
}
