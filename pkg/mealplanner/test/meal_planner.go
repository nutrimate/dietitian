package mealplannertest

import (
	"context"

	"github.com/stretchr/testify/mock"

	"gitlab.com/nutrimate/dietitian/pkg/mealplanner"
)

// MockMealPlanner is a mock of mealplanner.MealPlanner
type MockMealPlanner struct {
	mock.Mock
}

// PlanDiet is a mock method
func (m *MockMealPlanner) PlanDiet(ctx context.Context, request mealplanner.PlanDietRequest) ([]mealplanner.DietEntry, error) {
	args := m.Called(ctx, request)

	return args.Get(0).([]mealplanner.DietEntry), args.Error(1)
}
