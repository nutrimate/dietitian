package mealplanner

import (
	"fmt"

	"gitlab.com/nutrimate/dietitian/pkg/domainerrors"
)

var (
	// ErrInvalidDietGoal is a validation error returned when diet goal is not recognized
	ErrInvalidDietGoal = fmt.Errorf("%w: invalid diet goal", domainerrors.ErrValidation)
)

// Diet goals accepted by meal planner
const (
	LoseWeightDietGoal    = "LoseWeight"
	SustainWeightDietGoal = "SustainWeight"
	GainWeightDietGoal    = "GainWeight"
)

// DietGoal determines where boundaries of calorie intake fall
type DietGoal string

// NewDietGoal creates a new DietGoal validating input
func NewDietGoal(source string) (DietGoal, error) {
	var validValues = []string{
		LoseWeightDietGoal,
		SustainWeightDietGoal,
		GainWeightDietGoal,
	}

	for _, validValue := range validValues {
		if source == validValue {
			return DietGoal(source), nil
		}
	}

	return "", ErrInvalidDietGoal
}
