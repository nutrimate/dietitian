package mealplanner_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/dietitian/pkg/mealplanner"
)

func TestNewDays(t *testing.T) {
	t.Run("should return error when days are outside of allowed range", func(t *testing.T) {
		values := []uint8{
			0,
			11,
			15,
		}

		for _, input := range values {
			t.Run(fmt.Sprintf("input: %d", input), func(t *testing.T) {
				_, err := mealplanner.NewDays(input)

				assert.Equal(t, mealplanner.ErrDaysOutsideAllowedRange, err)
			})
		}
	})

	t.Run("should create diet days when input is inside allowed range", func(t *testing.T) {
		values := []uint8{
			1,
			5,
			10,
		}

		for _, input := range values {
			t.Run(fmt.Sprintf("input: %d", input), func(t *testing.T) {
				result, err := mealplanner.NewDays(input)

				if assert.NoError(t, err) {
					assert.Equal(t, input, uint8(result))
				}
			})
		}
	})
}
