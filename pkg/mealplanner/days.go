package mealplanner

import (
	"fmt"

	"gitlab.com/nutrimate/dietitian/pkg/domainerrors"
)

var (
	// ErrDaysOutsideAllowedRange is a validation error returned when input days are outside of valid range
	ErrDaysOutsideAllowedRange = fmt.Errorf("%w: days are outside allowed range", domainerrors.ErrValidation)
)

// Days is a number of days the diet should span for
type Days uint8

// NewDays creates number of diet days validating that number according to meal planner accepted range
func NewDays(source uint8) (Days, error) {
	if source < 1 || source > 10 {
		return 0, ErrDaysOutsideAllowedRange
	}

	return Days(source), nil
}
