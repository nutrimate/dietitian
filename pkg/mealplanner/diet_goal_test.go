package mealplanner_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/dietitian/pkg/mealplanner"
)

func TestNewDietGoal(t *testing.T) {
	t.Run("should return error when input is an invalid goal", func(t *testing.T) {
		values := []string{
			"",
			"invalid",
		}

		for _, input := range values {
			t.Run(fmt.Sprintf("input: %s", input), func(t *testing.T) {
				_, err := mealplanner.NewDietGoal(input)

				assert.Equal(t, mealplanner.ErrInvalidDietGoal, err)
			})
		}
	})

	t.Run("should return diet goal when input is valid", func(t *testing.T) {
		values := []string{
			mealplanner.LoseWeightDietGoal,
			mealplanner.SustainWeightDietGoal,
			mealplanner.GainWeightDietGoal,
		}

		for _, input := range values {
			t.Run(fmt.Sprintf("input: %s", input), func(t *testing.T) {
				result, err := mealplanner.NewDietGoal(input)

				if assert.NoError(t, err) {
					assert.Equal(t, input, string(result))
				}
			})
		}
	})
}
