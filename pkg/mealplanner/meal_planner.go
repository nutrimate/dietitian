package mealplanner

import "context"

// MealPlanner is a service used for planning diet's meals
type MealPlanner interface {
	// PlanDiet plans meals for a given diet
	PlanDiet(ctx context.Context, request PlanDietRequest) ([]DietEntry, error)
}
