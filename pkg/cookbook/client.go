package cookbook

import (
	"context"

	"gitlab.com/nutrimate/dietitian/pkg/recipe"
)

// Client is a service client for accessing cookbook microservice
type Client interface {
	GetAllRecipes(ctx context.Context) ([]recipe.Recipe, error)
}
