package patient

import "github.com/google/uuid"

// ID is an UUID that identifies a patient
type ID uuid.UUID

// Patient is patient
type Patient struct {
	ID        ID
	Firstname string
	Lastname  string
	Email     string
}
