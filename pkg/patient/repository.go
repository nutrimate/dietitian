package patient

import "context"

// Repository is a generic repository for accessing patients
type Repository interface {
	FindOne(ctx context.Context, patientID ID) (Patient, error)
}
