# Dietitian

A microservice that provides diets to patients

## Development

### Private Go modules

To set up usage of private Go modules run:
```
git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"
go env -w GONOSUMDB="gitlab.com/nutrimate/microservice-kit"
```

### Configuration

To configure local development environment create `.env` file with following contents:
```
PORT=4000
DEBUG=true
DB_CONNECTION_STRING=postgres://dietitian_user:password@localhost:5000/dietitian?sslmode=disable
ALLOWED_ORIGINS=http://localhost:3010
AUTH_JWKS_URI=https://dietetykwchmurze-stage.eu.auth0.com/.well-known/jwks.json
```

Configure git to use ssh for GitLab 
```
git config --system url."ssh://git@gitlab.com/".insteadOf "https://gitlab.com/"
```

### Database Migration

Migrate the database by running 
```
migrate -path db/migrations -database "postgres://dietitian_user:password@0.0.0.0:5000/dietitian?sslmode=disable" up
```

