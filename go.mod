module gitlab.com/nutrimate/dietitian

go 1.16

require (
	cloud.google.com/go v0.78.0
	github.com/aws/aws-sdk-go-v2 v1.2.0
	github.com/aws/aws-sdk-go-v2/service/lambda v1.1.1
	github.com/etherlabsio/healthcheck v0.0.0-20191224061800-dd3d2fd8c3f6
	github.com/getsentry/sentry-go v0.9.0
	github.com/google/uuid v1.1.2
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/labstack/echo/v4 v4.2.0
	github.com/lib/pq v1.9.0
	github.com/stretchr/testify v1.7.0
	github.com/upper/db/v4 v4.1.0
	gitlab.com/nutrimate/microservice-kit v1.2.0
	golang.org/x/sys v0.0.0-20210303074136-134d130e1a04 // indirect
)
