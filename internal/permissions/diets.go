package permissions

// WriteDiets is a scope that allows users to create, update, and delete diets
const WriteDiets = "write:diets"
