package externaladapters

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/aws/aws-sdk-go-v2/service/lambda"

	"gitlab.com/nutrimate/dietitian/pkg/mealplanner"
)

type mealPlanner struct {
	functionName string
	lambdaClient *lambda.Client
}

type planDietResult struct {
	Ok     bool                    `json:"ok"`
	Diet   []mealplanner.DietEntry `json:"diet"`
	Errors []string                `json:"errors"`
}

func (m mealPlanner) PlanDiet(ctx context.Context, request mealplanner.PlanDietRequest) ([]mealplanner.DietEntry, error) {
	payload, err := json.Marshal(request)
	if err != nil {
		return nil, fmt.Errorf("cannot marshal request: %w", err)
	}

	input := lambda.InvokeInput{
		FunctionName: &m.functionName,
		Payload:      payload,
	}
	output, err := m.lambdaClient.Invoke(ctx, &input)
	if err != nil {
		return nil, fmt.Errorf("failed to invoke lambda: %w", err)
	}

	var result planDietResult
	err = json.Unmarshal(output.Payload, &result)
	if !result.Ok {
		return nil, fmt.Errorf("diet planning failed: %s", strings.Join(result.Errors, ", "))
	}

	return result.Diet, nil
}

// NewMealPlannerClient create a new client meal planner that invokes AWS Lambda function
func NewMealPlannerClient(lambdaClient *lambda.Client, functionName string) mealplanner.MealPlanner {
	return mealPlanner{
		functionName: functionName,
		lambdaClient: lambdaClient,
	}
}
