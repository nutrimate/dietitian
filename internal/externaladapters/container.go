package externaladapters

import (
	"net/http"
	"net/url"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/lambda"

	"gitlab.com/nutrimate/dietitian/internal/config"
	"gitlab.com/nutrimate/dietitian/pkg/cookbook"
	"gitlab.com/nutrimate/dietitian/pkg/mealplanner"
)

// Container is a container module that provides adapters to external services
type Container interface {
	Cookbook() cookbook.Client
	MealPlanner() mealplanner.MealPlanner
}

type container struct {
	config config.Config
	lambda *lambda.Client
}

// Cookbook returns new http cookbook.Client
func (c container) Cookbook() cookbook.Client {
	httpClient := &http.Client{
		Timeout: time.Second * 30,
	}
	cookbookURL := url.URL(c.config.CookbookURL)

	return NewCookbookClient(httpClient, cookbookURL)
}

// MealPlanner returns new AWS Lambda client for mealplanner.MealPlanner
func (c container) MealPlanner() mealplanner.MealPlanner {
	return NewMealPlannerClient(c.lambda, c.config.MealPlannerFunctionName)
}

// NewContainer creates new container for external adapters
func NewContainer(config config.Config, awsConfig aws.Config) Container {
	l := lambda.NewFromConfig(awsConfig)

	return container{
		config: config,
		lambda: l,
	}
}
