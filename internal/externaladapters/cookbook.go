package externaladapters

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"gitlab.com/nutrimate/microservice-kit/pkg/reqctx"

	"gitlab.com/nutrimate/dietitian/pkg/cookbook"
	"gitlab.com/nutrimate/dietitian/pkg/recipe"
)

type cookbookClient struct {
	httpClient *http.Client
	baseURL    url.URL
}

// GetAllRecipes fetches recipes from cookbook
func (c cookbookClient) GetAllRecipes(ctx context.Context) ([]recipe.Recipe, error) {
	endpoint := c.baseURL.ResolveReference(&url.URL{
		Path:     "/recipes",
		RawQuery: "compact=true",
	})
	req, err := http.NewRequest("GET", endpoint.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("creating request failed: %w", err)
	}

	token := reqctx.GetToken(ctx)
	if token != nil {
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", *token))
	}

	res, err := c.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to execute request: %w", err)
	}

	if res.Body != nil {
		defer res.Body.Close()
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read response body: %w", err)
	}

	var recipes []recipe.Recipe
	err = json.Unmarshal(body, &recipes)
	if err != nil {
		return nil, fmt.Errorf("failed to parse recipes: %w", err)
	}

	return recipes, nil
}

// NewCookbookClient creates HTTP client for accessing cookbook microservice
func NewCookbookClient(httpClient *http.Client, baseURL url.URL) cookbook.Client {
	return cookbookClient{
		httpClient: httpClient,
		baseURL:    baseURL,
	}
}
