package api

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/nutrimate/microservice-kit/pkg/api"

	"gitlab.com/nutrimate/dietitian/internal/config"
	"gitlab.com/nutrimate/dietitian/internal/controllers"
)

// NewServer creates new API server
func NewServer(config config.Config, controllers controllers.Container) *echo.Echo {
	e := api.NewWithMiddleware()
	e.Debug = config.Debug
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: config.AllowedOrigins,
	}))

	api.RegisterControllers(e, []api.Controller{
		controllers.HealthCheckController(),
		controllers.DietsController(),
	})

	return e
}
