package api

import (
	"fmt"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/labstack/echo/v4"
	"github.com/upper/db/v4"
	postgres "github.com/upper/db/v4/adapter/postgresql"
	"gitlab.com/nutrimate/microservice-kit/pkg/auth"

	"gitlab.com/nutrimate/dietitian/internal/config"
	"gitlab.com/nutrimate/dietitian/internal/controllers"
	"gitlab.com/nutrimate/dietitian/internal/externaladapters"
	"gitlab.com/nutrimate/dietitian/internal/repositories"
)

// Container is a container module that provides API server components
type Container interface {
	Config() config.Config
	Server() *echo.Echo
}

type container struct {
	config      config.Config
	controllers controllers.Container
}

// Server returns new API server
func (c container) Server() *echo.Echo {
	return NewServer(c.config, c.controllers)
}

// Config returns existing config
func (c container) Config() config.Config {
	return c.config
}

// NewDependencyContainer creates a container with dependencies for api server
func NewDependencyContainer() (Container, error) {
	runtimeConfig, err := config.Load()
	if err != nil {
		return container{}, fmt.Errorf("could not load config: %w", err)
	}

	dbSession, err := openDbSession(runtimeConfig)
	if err != nil {
		return container{}, err
	}

	awsConfig := setupAWSConfig(runtimeConfig)
	authMiddleware := auth.NewMiddleware(auth.MiddlewareConfig{
		JWKSEndpoint: runtimeConfig.Auth.JWKSURI,
	})

	repositoriesContainer := repositories.NewContainer(dbSession)
	externalAdaptersContainer := externaladapters.NewContainer(runtimeConfig, awsConfig.Copy())
	controllersContainer := controllers.NewContainer(authMiddleware, repositoriesContainer, externalAdaptersContainer)

	c := container{
		config:      runtimeConfig,
		controllers: controllersContainer,
	}

	return c, nil
}

func openDbSession(config config.Config) (db.Session, error) {
	connectionURL, err := postgres.ParseURL(config.DbConnectionString)
	if err != nil {
		return nil, fmt.Errorf("invalid db connection string: %w", err)
	}

	session, err := postgres.Open(connectionURL)
	if err != nil {
		return nil, fmt.Errorf("could not open database connection: %w", err)
	}

	return session, nil
}

func setupAWSConfig(config config.Config) *aws.Config {
	awsConfig := aws.NewConfig()

	if config.AwsLambdaEndpointOverride != nil {
		awsConfig.EndpointResolver = aws.EndpointResolverFunc(func(service, region string) (aws.Endpoint, error) {
			if service == "lambda" {
				e := aws.Endpoint{
					URL:               *config.AwsLambdaEndpointOverride,
					HostnameImmutable: true,
					Source:            aws.EndpointSourceCustom,
				}
				return e, nil
			}

			return aws.Endpoint{}, &aws.EndpointNotFoundError{}
		})
	}

	return awsConfig
}
