package config

import (
	"fmt"
	"net/url"
)

// URLDecoder is an envconfig.Decoder for urls
type URLDecoder url.URL

// Decode parses a url from provided string
func (u *URLDecoder) Decode(value string) error {
	parsed, err := url.Parse(value)
	if err != nil {
		return fmt.Errorf("failed to decode url: %w", err)
	}

	if parsed == nil {
		return fmt.Errorf("parsed url is nil")
	}

	*u = URLDecoder(*parsed)
	return nil
}
