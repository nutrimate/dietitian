package repositories

import (
	"github.com/upper/db/v4"

	"gitlab.com/nutrimate/dietitian/pkg/diet"
	"gitlab.com/nutrimate/dietitian/pkg/patient"
)

// Container is a container module that provides repositories
type Container interface {
	DBSession() db.Session
	DietRepository() diet.Repository
	PatientRepository() patient.Repository
}

type container struct {
	dbSession db.Session
}

// DBSession returns existing db session
func (c container) DBSession() db.Session {
	return c.dbSession
}

// DietRepository returns existing diets repository
func (c container) DietRepository() diet.Repository {
	return NewDietRepository(c.dbSession)
}

func (c container) PatientRepository() patient.Repository {
	return NewPatientRepository(c.dbSession)
}

// NewContainer creates a new container with repositories
func NewContainer(dbSession db.Session) Container {
	c := container{
		dbSession: dbSession,
	}

	return c
}
