package dbmodels

import (
	"database/sql/driver"
	"time"

	"github.com/google/uuid"
	postgres "github.com/upper/db/v4/adapter/postgresql"

	"gitlab.com/nutrimate/dietitian/pkg/diet"
)

// MealSchedule represents diet.MealSchedule stored in database as JSONB
type MealSchedule diet.MealSchedule

// Value implements valuer interface for postgres driver
func (s MealSchedule) Value() (driver.Value, error) {
	return postgres.JSONBValue(s)
}

// Scan implements scanner interface for postgres driver
func (s *MealSchedule) Scan(src interface{}) error {
	return postgres.ScanJSONB(s, src)
}

// Diet represents diet.WeeklyDiet stored in database
type Diet struct {
	ID           uuid.UUID       `db:"id"`
	PatientID    uuid.UUID       `db:"patient_id"`
	StartDay     time.Time       `db:"start_day"`
	EndDay       time.Time       `db:"end_day"`
	MealSchedule MealSchedule    `db:"meal_schedule"`
	MealPlan     []MealPlanEntry `db:","`
}

var dietDbTimezone = time.UTC

// NewDietFromEntity creates database model of diet from domain entity
func NewDietFromEntity(d diet.Diet) Diet {
	dietID := uuid.UUID(d.ID)

	mealPlanEntries := make([]MealPlanEntry, len(d.MealPlan))
	for i, e := range d.MealPlan {
		mealPlanEntries[i] = MealPlanEntry{
			DietID:     dietID,
			Day:        e.Day,
			MealTypeID: uuid.UUID(e.MealTypeID),
			RecipeID:   uuid.UUID(e.RecipeID),
		}
	}

	return Diet{
		ID:           dietID,
		PatientID:    uuid.UUID(d.PatientID),
		StartDay:     d.Interval.StartDay.In(dietDbTimezone),
		EndDay:       d.Interval.EndDay.In(dietDbTimezone),
		MealSchedule: MealSchedule(d.MealSchedule),
		MealPlan:     nil,
	}
}

// MealPlanEntry represents diet.MealPlanEntry stored in database
type MealPlanEntry struct {
	DietID     uuid.UUID `db:"diet_id"`
	Day        uint8     `db:"day"`
	MealTypeID uuid.UUID `db:"meal_type_id"`
	RecipeID   uuid.UUID `db:"recipe_id"`
}
