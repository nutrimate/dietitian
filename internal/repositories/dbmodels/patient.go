package dbmodels

import (
	"github.com/google/uuid"
	"gitlab.com/nutrimate/dietitian/pkg/patient"
)

// Patient represents Patient stored in database
type Patient struct {
	ID        uuid.UUID `db:"id"`
	Firstname string    `db:"firstname"`
	Lastname  string    `db:"lastname"`
	Email     string    `db:"email"`
}

// NewPatientFromEntity creates database model of patient from domain entity
func NewPatientFromEntity(p patient.Patient) Patient {
	patientID := uuid.UUID(p.ID)

	return Patient{
		ID:        patientID,
		Firstname: p.Firstname,
		Lastname:  p.Lastname,
		Email:     p.Email,
	}
}
