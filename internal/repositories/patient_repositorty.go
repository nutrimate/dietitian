package repositories

import (
	"context"
	"fmt"

	"github.com/upper/db/v4"
	"gitlab.com/nutrimate/dietitian/pkg/patient"
)

const patientsTable = "patients"

type patientDbRepository struct {
	session  func(ctx context.Context) db.Session
	patients string
}

// Find gets patient from database by id
func (r patientDbRepository) FindOne(ctx context.Context, patientID patient.ID) (patient.Patient, error) {
	var result patient.Patient
	err := r.session(ctx).Collection(patientsTable).Find(patientID).One(&result)

	if err != nil {
		return result, fmt.Errorf("failed to query patient: %w", err)
	}

	return result, nil
}

// NewPatientRepository creates patients database repository
func NewPatientRepository(session db.Session) patient.Repository {
	return &patientDbRepository{
		session: func(ctx context.Context) db.Session {
			return session.WithContext(ctx)
		},
	}
}
