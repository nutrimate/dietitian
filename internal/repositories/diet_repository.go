package repositories

import (
	"context"
	"fmt"

	"github.com/upper/db/v4"

	"gitlab.com/nutrimate/dietitian/internal/repositories/dbmodels"
	"gitlab.com/nutrimate/dietitian/pkg/diet"
)

const dietsTable = "diets"
const dietMealPlanEntriesTable = "diet_meal_plan_entries"

type dietDbRepository struct {
	session func(ctx context.Context) db.Session
	diets   string
}

// Create inserts new diets to database
func (r dietDbRepository) Create(ctx context.Context, d diet.Diet) error {
	return r.session(ctx).Tx(func(s db.Session) error {
		dbDiet := dbmodels.NewDietFromEntity(d)

		_, err := s.Collection(dietsTable).Insert(dbDiet)

		if err != nil {
			return fmt.Errorf("failed to insert diet: %w", err)
		}

		insertMealPlanEntries := s.SQL().InsertInto(dietMealPlanEntriesTable)
		for _, e := range dbDiet.MealPlan {
			insertMealPlanEntries.Values(e)
		}

		_, err = insertMealPlanEntries.Exec()

		if err != nil {
			return fmt.Errorf("failed to insert diet meal plan: %w", err)
		}

		return nil
	})
}

// NewDietRepository creates diets database repository
func NewDietRepository(session db.Session) diet.Repository {
	return &dietDbRepository{
		session: func(ctx context.Context) db.Session {
			return session.WithContext(ctx)
		},
	}
}
