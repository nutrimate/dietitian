package healthctrl

import (
	"github.com/etherlabsio/healthcheck"
	"github.com/labstack/echo/v4"
	"gitlab.com/nutrimate/microservice-kit/pkg/api"
)

type healthCheckController struct {
}

func (ctrl healthCheckController) checkHealth(c echo.Context) error {
	// TODO: Check db connection
	handler := healthcheck.HandlerFunc()

	return echo.WrapHandler(handler)(c)
}

// Register add handlers for /health routes
func (ctrl healthCheckController) Register(e *echo.Echo) {
	health := e.Group("/health")
	{
		health.GET("", ctrl.checkHealth)
	}
}

// NewHealthCheckController creates a new controller for health check routes
func NewHealthCheckController() api.Controller {
	return healthCheckController{}
}
