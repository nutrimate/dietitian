package controllers

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/nutrimate/microservice-kit/pkg/api"

	dietcontroller "gitlab.com/nutrimate/dietitian/internal/controllers/dietctrl"
	"gitlab.com/nutrimate/dietitian/internal/controllers/healthctrl"
	"gitlab.com/nutrimate/dietitian/internal/externaladapters"
	"gitlab.com/nutrimate/dietitian/internal/repositories"
	"gitlab.com/nutrimate/dietitian/pkg/diet"
	"gitlab.com/nutrimate/dietitian/pkg/dietitian"
)

// Container is a container module that provides controllers
type Container interface {
	DietsController() api.Controller
	HealthCheckController() api.Controller
}

type container struct {
	authMiddleware   echo.MiddlewareFunc
	repositories     repositories.Container
	externalAdapters externaladapters.Container
}

// DietsController returns new diets controller
func (c container) DietsController() api.Controller {
	dietRepository := c.repositories.DietRepository()

	cookbook := c.externalAdapters.Cookbook()
	mealPlanner := c.externalAdapters.MealPlanner()

	dietitianService := dietitian.NewService()
	recipesService := dietitian.NewRecipesService(cookbook)
	service := diet.NewService(dietRepository, dietitianService, recipesService, mealPlanner)

	return dietcontroller.New(c.authMiddleware, service)
}

// HealthCheckController returns new health check controller
func (c container) HealthCheckController() api.Controller {
	return healthctrl.NewHealthCheckController()
}

// NewContainer creates a new container with controllers
func NewContainer(authMiddleware echo.MiddlewareFunc, repositories repositories.Container, adapters externaladapters.Container) Container {
	return container{
		authMiddleware:   authMiddleware,
		repositories:     repositories,
		externalAdapters: adapters,
	}
}
