package dietcontroller_test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"cloud.google.com/go/civil"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/nutrimate/microservice-kit/pkg/apitest"
	"gitlab.com/nutrimate/microservice-kit/pkg/auth"

	dietcontroller "gitlab.com/nutrimate/dietitian/internal/controllers/dietctrl"
	"gitlab.com/nutrimate/dietitian/internal/permissions"
	"gitlab.com/nutrimate/dietitian/pkg/diet/commands"
	diettest "gitlab.com/nutrimate/dietitian/pkg/diet/test"
	"gitlab.com/nutrimate/dietitian/pkg/mealplanner"
)

type textContext struct {
	server  *echo.Echo
	service *diettest.MockService
}

func setup() textContext {
	authMiddleware := apitest.MockAuthMiddleware()
	service := &diettest.MockService{}
	controller := dietcontroller.New(authMiddleware, service)

	server := apitest.SetupTestAPIServer(controller)

	return textContext{
		server:  server,
		service: service,
	}
}

func writeDietsUser() auth.RequestUser {
	return auth.RequestUser{
		ID: "user-id",
		Permissions: []string{
			permissions.WriteDiets,
		},
		Scope: "",
	}
}

func TestNew(t *testing.T) {
	t.Run("POST /diets", func(t *testing.T) {
		method := "POST"
		path := "/diets"

		accessTest := apitest.NewAccessTest(func() *echo.Echo {
			return setup().server
		})

		accessTest.ShouldCheckPermissions(t, method, path)
		accessTest.ShouldUseAuthMiddleware(t, method, path)

		t.Run("should create diet when request is valid", func(t *testing.T) {
			// Given
			s := setup()

			dietID := uuid.New()
			patientID := uuid.New()
			startDay := civil.DateOf(time.Now())
			mealSchedule := []uuid.UUID{
				uuid.New(),
				uuid.New(),
			}
			request := dietcontroller.CreateDietRequest{
				DietID:             dietID.String(),
				PatientID:          patientID.String(),
				Goal:               mealplanner.LoseWeightDietGoal,
				DailyCalorieIntake: 2000,
				DailyMinProteins:   60,
				DailyMaxProteins:   240,
				DailyMinFats:       50,
				StartDay:           startDay.String(),
				NumberOfDays:       3,
				MealSchedule: []string{
					mealSchedule[0].String(),
					mealSchedule[1].String(),
				},
			}
			payload, err := json.Marshal(request)
			require.NoError(t, err)

			service := s.service
			service.On("CreateNewDiet", mock.Anything, mock.IsType(commands.CreateNewDiet{})).Return(nil)

			user := writeDietsUser()
			req := apitest.NewAuthRequestWithUser(user, method, path, payload)
			rec := httptest.NewRecorder()
			server := s.server

			// When
			server.ServeHTTP(rec, req)

			// Then
			expectedCommand := commands.CreateNewDiet{
				UserID:             user.ID,
				DietID:             dietID,
				PatientID:          patientID,
				StartDay:           startDay,
				NumberOfDays:       request.NumberOfDays,
				Goal:               request.Goal,
				DailyCalorieIntake: request.DailyCalorieIntake,
				DailyMinProteins:   request.DailyMinProteins,
				DailyMaxProteins:   request.DailyMaxProteins,
				DailyMinFats:       request.DailyMinFats,
				MealSchedule:       mealSchedule,
			}
			assert.Equal(t, http.StatusNoContent, rec.Code, rec.Body.String())
			service.AssertCalled(t, "CreateNewDiet", mock.Anything, expectedCommand)
		})
	})
}
