package dietcontroller

import (
	"fmt"
	"net/http"

	"cloud.google.com/go/civil"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gitlab.com/nutrimate/microservice-kit/pkg/api"
	"gitlab.com/nutrimate/microservice-kit/pkg/auth"
	"gitlab.com/nutrimate/microservice-kit/pkg/reqctx"

	"gitlab.com/nutrimate/dietitian/internal/permissions"
	"gitlab.com/nutrimate/dietitian/pkg/diet"
	"gitlab.com/nutrimate/dietitian/pkg/diet/commands"
)

type dietsController struct {
	authMiddleware echo.MiddlewareFunc
	service        diet.Service
}

func (ctrl dietsController) create(c echo.Context) error {
	user := auth.GetRequestUser(c)
	if user == nil {
		return echo.ErrUnauthorized
	}

	var request CreateDietRequest
	err := c.Bind(&request)
	if err != nil {
		return err
	}

	dietID, err := uuid.Parse(request.DietID)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("invalid diet id: %v", err))
	}

	patientID, err := uuid.Parse(request.PatientID)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("invalid patient id: %v", err))
	}

	mealSchedule := make([]uuid.UUID, len(request.MealSchedule))
	for i, m := range request.MealSchedule {
		mealTypeID, err := uuid.Parse(m)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, fmt.Errorf("invalid meal type id: %v", err))
		}
		mealSchedule[i] = mealTypeID
	}

	startDay, err := civil.ParseDate(request.StartDay)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("invalid start date: %v", err))
	}

	ctx := reqctx.Create(c)
	command := commands.CreateNewDiet{
		UserID:             user.ID,
		DietID:             dietID,
		PatientID:          patientID,
		MealSchedule:       mealSchedule,
		StartDay:           startDay,
		NumberOfDays:       request.NumberOfDays,
		Goal:               request.Goal,
		DailyCalorieIntake: request.DailyCalorieIntake,
		DailyMinProteins:   request.DailyMinProteins,
		DailyMaxProteins:   request.DailyMaxProteins,
		DailyMinFats:       request.DailyMinFats,
	}

	err = ctrl.service.CreateNewDiet(ctx, command)
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}

// Register add handlers for /diets routes
func (ctrl dietsController) Register(e *echo.Echo) {
	diets := e.Group("/diets")
	diets.Use(ctrl.authMiddleware)
	{
		diets.POST("", ctrl.create, auth.NewPermissionsMiddleware(auth.Permissions{
			All: []string{
				permissions.WriteDiets,
			},
		}))
	}
}

// New creates a new controller for diets routes
func New(authMiddleware echo.MiddlewareFunc, service diet.Service) api.Controller {
	return dietsController{
		authMiddleware: authMiddleware,
		service:        service,
	}
}
