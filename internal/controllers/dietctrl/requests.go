package dietcontroller

// CreateDietRequest represents payload sent when creating a diets
type CreateDietRequest struct {
	DietID             string   `json:"dietId" validate:"required,uuid"`
	PatientID          string   `json:"patientId" validate:"required,uuid"`
	Goal               string   `json:"goal" validate:"required"`
	DailyCalorieIntake float64  `json:"dailyCalorieIntake" validate:"gt=0"`
	DailyMinProteins   float64  `json:"dailyMinProteins" validate:"gt=0"`
	DailyMaxProteins   float64  `json:"dailyMaxProteins" validate:"gt=0"`
	DailyMinFats       float64  `json:"dailyMinFats" validate:"gt=0"`
	StartDay           string   `json:"startDay" validate:"required,datetime=2006-01-02"`
	NumberOfDays       uint8    `json:"numberOfDays" validate:"gt=0,lte=14"`
	MealSchedule       []string `json:"mealSchedule" validate:"required,min=1,dive,required,uuid"`
}
