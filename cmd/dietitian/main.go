package main

import (
	"fmt"
	"log"
	"time"

	"github.com/getsentry/sentry-go"
	_ "github.com/joho/godotenv/autoload"
	_ "github.com/lib/pq"

	"gitlab.com/nutrimate/dietitian/internal/api"
)

func startServer() error {
	container, err := api.NewDependencyContainer()
	if err != nil {
		return fmt.Errorf("could not create dependency container: %w", err)
	}

	config := container.Config()
	apiServer := container.Server()
	addr := fmt.Sprintf(":%d", config.Port)

	err = apiServer.Start(addr)
	if err != nil {
		return fmt.Errorf("server failed: %w", err)
	}

	return nil
}

func main() {
	err := sentry.Init(sentry.ClientOptions{})
	if err != nil {
		log.Fatalf("sentry.Init: %s", err)
	}
	defer sentry.Flush(2 * time.Second)

	err = startServer()
	if err != nil {
		sentry.CaptureException(err)
		log.Fatalf("%v", err)
	}
}
