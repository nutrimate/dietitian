CREATE EXTENSION IF NOT EXISTS "btree_gist";

DROP TABLE diets;

CREATE TABLE patients
(
    id UUID NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE diets
(
    id            UUID  NOT NULL,
    patient_id    UUID  NOT NULL,
    start_date    DATE  NOT NULL,
    end_date      DATE  NOT NULL CHECK (diets.start_date < end_date),
    meal_schedule JSONB NOT NULL,

    PRIMARY KEY (id),
    FOREIGN KEY (patient_id) REFERENCES patients(id),
    CONSTRAINT overlapping_times EXCLUDE USING GIST (
        patient_id WITH =,
        daterange(start_date, end_date) WITH &&
    )
);

CREATE TABLE diet_meal_plan_entries
(
    diet_id      UUID     NOT NULL,
    day          SMALLINT NOT NULL CHECK (day > 0 AND day < 256),
    meal_type_id UUID     NOT NULL,
    recipe_id    UUID     NOT NULL,


    PRIMARY KEY (diet_id, day, meal_type_id),
    FOREIGN KEY (diet_id) REFERENCES diets (id)
)
