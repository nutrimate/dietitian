DROP TABLE diet_meal_plan_entries;
DROP TABLE diets;
DROP TABLE patients;

CREATE TABLE diets (
    id UUID NOT NULL,
    user_id VARCHAR(255) NOT NULL,
    week_start_day DATE NOT NULL,
    target_calories NUMERIC(8,4) NOT NULL,
    meal_schedule JSONB NOT NULL,
    meal_plan JSONB NOT NULL,
    week_start_day timestamp with time zone,

    PRIMARY KEY (id),
    UNIQUE (user_id, week_start_day)
);