BEGIN;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE diets (
  id UUID NOT NULL,
  user_id VARCHAR(255) NOT NULL,
  week_start_day DATE NOT NULL,
  target_calories NUMERIC(8,4) NOT NULL,
  meal_schedule JSONB NOT NULL,
  meal_plan JSONB NOT NULL,

  PRIMARY KEY (id),
  UNIQUE (user_id, week_start_day)
);

COMMIT;
